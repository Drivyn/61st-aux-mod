#include "script_component.hpp"

class CfgPatches {
    class ADDON {
        name = QUOTE(COMPONENT);
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {};
        author = "AUTHOR";
        VERSION_CONFIG;
    };
};

class CfgWeapons {
    class rhs_uniform_acu_ucp;
    class rhs_uniform_acu_oefcp : rhs_uniform_acu_ucp {
        class ItemInfo;
    };
    class NEW_UNIFORM_WEP(inf,base) : rhs_uniform_acu_oefcp {
        displayName = "[61st] Infantry Uniform";
        class ItemInfo : ItemInfo {
            uniformClass = QUOTE(NEW_UNIFORM(inf,base));
        };
    };
    ///////////////////////////////////////////INF Uniforms/////////////////////////////////////////////////////////
    NEW_INF_UNIFORM_WEP(Myers)
    NEW_INF_UNIFORM_WEP(1SG)
    NEW_INF_UNIFORM_WEP(2LT)
    NEW_INF_UNIFORM_WEP(CPL)
    NEW_INF_UNIFORM_WEP(CW1)
    NEW_INF_UNIFORM_WEP(CW2)
    NEW_INF_UNIFORM_WEP(CW3)
    NEW_INF_UNIFORM_WEP(CW4)
    NEW_INF_UNIFORM_WEP(CW5)
    NEW_INF_UNIFORM_WEP(1LT)
    NEW_INF_UNIFORM_WEP(PFC)
    NEW_INF_UNIFORM_WEP(PV2)
    NEW_INF_UNIFORM_WEP(PVT)
    NEW_INF_UNIFORM_WEP(SFC)
    NEW_INF_UNIFORM_WEP(SGT)
    NEW_INF_UNIFORM_WEP(SPC)
    NEW_INF_UNIFORM_WEP(SSG)
    NEW_INF_UNIFORM_WEP(Avian)
    NEW_INF_UNIFORM_WEP(Sierra)
    NEW_INF_UNIFORM_WEP(Abstract)
    NEW_INF_UNIFORM_WEP(Carnage)
    NEW_INF_UNIFORM_WEP(Fry)
};


class CfgVehicles {
    class rhsusf_army_acu_oefcp_uniform;
    class NEW_UNIFORM(inf,base) : rhsusf_army_acu_oefcp_uniform {
        hiddenSelectionsTextures[] = {QPATHTOF(_textures\inf\base.paa),"rhsusf\addons\rhsusf_infantry2\acu\data\rhsusf_uniform_acu_02_oefcp_co.paa","rhsusf\addons\rhsusf_infantry2\acu\data\rhsusf_uniform_acu_03_ucp_co.paa","#(argb,8,8,3)color(0,0,0,0)","rhsusf\addons\rhsusf_infantry2\acu\data\rhsusf_uniform_acu_acc2_co.paa"};
    };


    ///////////////////////////////////////////INF Uniforms/////////////////////////////////////////////////////////
    NEW_INF_UNIFORM(Myers)
    NEW_INF_UNIFORM(1SG)
    NEW_INF_UNIFORM(2LT)
    NEW_INF_UNIFORM(CPL)
    NEW_INF_UNIFORM(CW1)
    NEW_INF_UNIFORM(CW2)
    NEW_INF_UNIFORM(CW3)
    NEW_INF_UNIFORM(CW4)
    NEW_INF_UNIFORM(CW5)
    NEW_INF_UNIFORM(1LT)
    NEW_INF_UNIFORM(PFC)
    NEW_INF_UNIFORM(PV2)
    NEW_INF_UNIFORM(PVT)
    NEW_INF_UNIFORM(SFC)
    NEW_INF_UNIFORM(SGT)
    NEW_INF_UNIFORM(SPC)
    NEW_INF_UNIFORM(SSG)
    NEW_INF_UNIFORM(Avian)
    NEW_INF_UNIFORM(Sierra)
    NEW_INF_UNIFORM(Abstract)
    NEW_INF_UNIFORM(Carnage)
    NEW_INF_UNIFORM(Fry)
};